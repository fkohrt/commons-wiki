WIKI_DIR := content
BUILD_DIR := public

CONF_SRC = $(wildcard $(WIKI_DIR)/config/*)
CONF = $(CONF_SRC:$(WIKI_DIR)/config/%=$(WIKI_DIR)/tiddlers/%)
CONF_QUOTED = $(patsubst $(WIKI_DIR)/config/%,'$(WIKI_DIR)/tiddlers/%',$(CONF_SRC))

.PHONY: all base clean

all: $(BUILD_DIR)/index.html

base:
	$(MAKE) -C tw-base/

clean:
	rm $(BUILD_DIR)/index.html
	rm $(CONF_QUOTED)
	rm -rf tw-base/

$(BUILD_DIR)/index.html: $(BUILD_DIR)/ $(CONF) base
	tiddlywiki $(WIKI_DIR)/ --output $(BUILD_DIR)/ --build index

$(BUILD_DIR)/:
	mkdir $@

$(CONF):
	cp '$(WIKI_DIR)/config/$(@F)' '$@'

