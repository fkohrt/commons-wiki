# Commons-Wiki

In meinem Commons-Wiki dokumentiere ich, unter anderem, mein Praktikum beim [Commons-Institut](#Commons-Institut). Und alles andere, was mit [Commoning](#Commoning) zu tun hat.

## Ansehen

Unter [`fkohrt.gitlab.io/commons-wiki/`](https://fkohrt.gitlab.io/commons-wiki/) kann man sich das Wiki angucken.

## Mitmachen

Warum nicht? Ich bin offen für andere Beiträge. Unter [`gitlab.com/fkohrt/commons-wiki`](https://gitlab.com/fkohrt/commons-wiki) kann der Quelltext eingesehen und verändert werden. Mehr Infos gibt es unter [CONTRIBUTING](#CONTRIBUTING).

## Kopieren und verteilen

Na klar! Sofern nicht anders gekennzeichnet, ist alles hier lizenziert unter [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de) (oder neuer). Klar davon ausgenommen sind Zitate Dritter.

![CC BY-SA 4.0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg)
