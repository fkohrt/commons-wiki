# Mitmachen

Ich bin offen für andere Beiträge. Dabei bitte folgende Metadaten-Felder berücksichtigen:

- `creator`/`modifier`: eindeutiger Autor\*innenbezeichner (z. B. _Florian_)
- `created`/`modified`: mindestens auf den Tag genaue Angabe der letzten Änderung im [TiddlyWiki-Format](https://tiddlywiki.com/#Date%20Fields)
- `tmap.id`: eindeutiger, zufällig erzeugter Bezeichner für einen Eintrag im [TiddlyMap-Format](http://tiddlymap.org/#Storage%20of%20Edges%20and%20their%20types)

"Eindeutig" meint dabei immer "eindeutig innerhalb dieses Commons-Wikis".
